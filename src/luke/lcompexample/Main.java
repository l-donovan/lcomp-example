package luke.lcompexample;

import luke.lcomp.base.LComp;
import luke.lcomp.base.Utils;
import luke.lcomp.base.Instruction;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        LComp comp = new LComp();

        // Register $02 holds the result of the most recent number in the Fibonacci sequence
        ArrayList<Instruction> fibonacci = Utils.parseInstructions(new String[] {
                "00100100000001010000000000001010", // ADDIU $05, $00, 10
                "00100100000000010000000000000000", // ADDIU $01, $00, 0
                "00100100000000100000000000000001", // ADDIU $02, $00, 1
                "00100100001000110000000000000000", // ADDIU $03, $01, 0
                "00100100010000010000000000000000", // ADDIU $01, $02, 0
                "00000000010000110001000000100001", // ADDU  $02, $02, $03
                "00100100100001000000000000000001", // ADDIU $04, $04, 1
                "00010000100001010000000000001001", // BEQ   $04, $05, 9
                "00001000000000000000000000000011", // J     3
                "00000000000000000000000000000000", // NOOP
        });

        comp.loadInstructions(fibonacci);

        comp.run();

        System.out.println(comp.getRegister(2));
    }
}
